package veating.bean;

import veating.bean.Bean;

public class Food extends Bean{
	private int fid;//ID
	private String foodname;//食物名称
	private String foodprice;//食物价格
	private String foodtype;//食物类型
	private String foodnum;//食物编号
	private String foodphoto;//食物图像
	private String sname;//店铺名
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public String getFoodname() {
		return foodname;
	}
	public void setFoodname(String foodname) {
		this.foodname = foodname;
	}
	public String getFoodprice() {
		return foodprice;
	}
	public void setFoodprice(String foodprice) {
		this.foodprice = foodprice;
	}
	public String getFoodtype() {
		return foodtype;
	}
	public void setFoodtype(String foodtype) {
		this.foodtype = foodtype;
	}
	public String getFoodnum() {
		return foodnum;
	}
	public void setFoodnum(String foodnum) {
		this.foodnum = foodnum;
	}
	public String getFoodphoto() {
		return foodphoto;
	}
	public void setFoodphoto(String foodphoto) {
		this.foodphoto = foodphoto;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}


}
