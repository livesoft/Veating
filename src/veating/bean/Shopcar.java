package veating.bean;

import veating.bean.Bean;

public class Shopcar extends Bean{
	private int scid;//ID
	private String spname;//店铺名
	private String fdname;//食物名
	private String foodprice;//食物价格
	private String foodmount;//食物个数
	private String count;//食物总计
	private String money;//应付金额
	public int getScid() {
		return scid;
	}
	public void setScid(int scid) {
		this.scid = scid;
	}
	public String getSpname() {
		return spname;
	}
	public void setSpname(String spname) {
		this.spname = spname;
	}
	public String getFdname() {
		return fdname;
	}
	public void setFdname(String fdname) {
		this.fdname = fdname;
	}
	public String getFoodprice() {
		return foodprice;
	}
	public void setFoodprice(String foodprice) {
		this.foodprice = foodprice;
	}
	public String getFoodmount() {
		return foodmount;
	}
	public void setFoodmount(String foodmount) {
		this.foodmount = foodmount;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}

}
