package veating.dao;

import veating.bean.Admin;

public interface AdminDao {
	/**
	 * 
	 * @param admin
	 * @return
	 */
	public int save(Admin admin);
	/**
	 * 
	 * @param aid
	 * @return
	 */
	public int delete(int aid);
	/**
	 * 
	 * @param aid
	 * @param admin
	 * @return
	 */
	public int update(int aid,Admin admin);
	

}
