package veating.dao;

import veating.bean.Food;

public interface FoodDao {
	/**
	 * 
	 * @param food
	 * @return
	 */
	public int save(Food food);
	/**
	 * 
	 * @param fid
	 * @return
	 */
	public int delete(int fid);
	/**
	 * 
	 * @param fid
	 * @param food
	 * @return
	 */
	public int update(int fid,Food food);

}
