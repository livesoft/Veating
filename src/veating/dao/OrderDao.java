package veating.dao;

import javax.persistence.criteria.Order;

public interface OrderDao {
	/**
	 * 
	 * @param order
	 * @return
	 */
	public int save(Order order);
	/**
	 * 
	 * @param oid
	 * @return
	 */
	public int delete(int oid);
	/**
	 * 
	 * @param oid
	 * @param order
	 * @return
	 */
	public int update(int oid,Order order);
	
	

}
