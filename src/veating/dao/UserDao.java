package veating.dao;

import veating.bean.User;

public interface UserDao {
	/**
	 * 
	 * @param user
	 * @return
	 */
	public int save(User user);
	/**
	 * 
	 * @param uid
	 * @return
	 */
	public int delete(int uid);
	/**
	 * 
	 * @param uid
	 * @param user
	 * @return
	 */
	public int update(int uid,User user);
}
