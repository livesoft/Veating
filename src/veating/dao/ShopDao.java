package veating.dao;

import veating.bean.Shop;

public interface ShopDao {
	/**
	 * 
	 * @param shop
	 * @return
	 */
	public int save(Shop shop);
	/**
	 * 
	 * @param sid
	 * @return
	 */
	public int delete(int sid);
	/**
	 * 
	 * @param sid
	 * @param shop
	 * @return
	 */
	public int update(int sid,Shop shop);

}
