package veating.dao;

import veating.bean.Shopcar;

public interface ShopcarDao {
	/**
	 * 
	 * @param shopcar
	 * @return
	 */
	public int save(Shopcar shopcar);
	/**
	 * 
	 * @param scid
	 * @return
	 */
	public int delete(int scid);
	/**
	 * 
	 * @param scid
	 * @param shopcar
	 * @return
	 */
	public int update(int scid,Shopcar shopcar);


}
