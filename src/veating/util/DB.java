package veating.util;
import java.sql.*;
import java.util.Properties;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSourceFactory;
public class DB {

	private static Connection conn = null;
	private static ResultSet rs = null;
	private static Statement s = null;
	private static final String URL = "jdbc:mysql://localhost:3306/veating";
	private static final String USER = "root";
	private static final String PASS = "123";
	private static BasicDataSource dataSource = null;
	
	/**
	 * 初始化数据库连接�?
	 */
	public static void init()
	{
		Properties prop = new Properties();  //Map
		prop.setProperty("url", URL);
		prop.setProperty("username", USER);
		prop.setProperty("password", PASS);
		prop.setProperty("driverClassName", "com.mysql.jdbc.Driver");
		prop.setProperty("maxActive", "10");
		prop.setProperty("maxIdle", "0");
		prop.setProperty("maxWait", "10000");
		
		if(dataSource !=null)
		{
			try {
				dataSource.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		try {
			dataSource = (BasicDataSource) BasicDataSourceFactory.createDataSource(prop);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 创建Connection
	 * @return
	 */
	public static Connection getConnection()
	{
//		try {
//			Class.forName("com.mysql.jdbc.Driver");//com.microsoft.sqlserver.jdbc.SQLServerDriver
//		} catch (ClassNotFoundException e) {
//			
//			e.printStackTrace();
//		}
//		try {
//			conn = DriverManager.getConnection(URL, USER, PASS);
//		} catch (SQLException e) {
//			
//			e.printStackTrace();
//		}
		
		if(dataSource == null)
			init();
		
		try {
				conn = dataSource.getConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
		
		
		return conn;
	}
	
	/**
	 * 执行INSERT/DELETE/UPDATE
	 * @param sql
	 * @return
	 */
	public static int RunSQL(String sql)
	{
		int result = 0;
		
		try {
			s = getConnection().createStatement();
			result = s.executeUpdate(sql);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return result;
	}
	/**
	 * 执行 SELECT
	 * @param sql
	 * @return  ResultSet类型 
	 */
	public static ResultSet RunSelect(String sql)
	{
		
		
		Statement s = null;
		try {
			s = getConnection().createStatement();
			System.out.println("sql:"+sql);
			rs = s.executeQuery(sql);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return rs;
	}
	
	public static void close()
	{
		if(rs!=null)
		{
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(s != null)
		{
			try {
				s.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(conn!=null)
		{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
