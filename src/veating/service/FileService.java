package veating.service;

import java.util.ArrayList;
/**
 * @author 
 * 保存文件地址
 * */
public class FileService {

	/**
	 * 多文件上传 
	 * */
	private static  ArrayList<String> filelist = new ArrayList<String>();
	
	private static  String staticfile = "";
	private static  String difficultPhotoPath = "";
	
	
	public static String getDifficultPhotoPath() {
		return difficultPhotoPath;
	}

	public static void setDifficultPhotoPath(String difficultPhotoPath) {
		FileService.difficultPhotoPath = difficultPhotoPath;
	}

	public static ArrayList<String> getFilelist() {
		return filelist;
	}

	public static void setFilelist(ArrayList<String> filelist) {
		FileService.filelist = filelist;
	}

	/**
	 * 单文件上传保存地址
	 * */
	public  String getStaticfile() {
		return staticfile;
	}

	public  void setStaticfile(String staticfile) {
		FileService.staticfile = staticfile;
	}

	
	
	

	
	
	
}
