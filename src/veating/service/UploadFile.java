package veating.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;


/**
 * 上传文件、照片等通用类
 * 
 * @author 
 */
public class UploadFile extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ArrayList<String> al = 
			FileService.getFilelist();
	
	public ArrayList<String> getAl() {
		return al;
	}
	public void setAl(ArrayList<String> al) {
		this.al = al;
	}

	/**
	 * 通用文件上传 
	 * */
	private File[] files ; 


	private String actionName ; 
	private String namespace ;
	private List<String> filepaths ; 
	private String[] filesContentType ;
	private String[] filesFileName ; 
	

	public File[] getFiles() {
		return files;
	}
	public void setFiles(File[] files) {
		this.files = files;
	}
	public String[] getFilesFileName() {
		return filesFileName;
	}
	public void setFilesFileName(String[] filesFileName) {
		this.filesFileName = filesFileName;
	}

	private String uptype;
	public String[] getFilesContentType() {
		return filesContentType;
	}
	public void setFilesContentType(String[] filesContentType) {
		this.filesContentType = filesContentType;
	}
	public String getUptype() {
		return uptype;
	}
	public void setUptype(String uptype) {
		this.uptype = uptype;
	}

	private File[] upload ; 
	private String[] uploadFileName ; 
	
	public List<String> getFilepaths() {
		return filepaths;
	}
	public void setFilepaths(List<String> filepaths) {
		this.filepaths = filepaths;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getNamespace() {
		return "/"+namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public File[] getUpload() {
		return upload;
	}
	public void setUpload(File[] upload) {
		this.upload = upload;
	}
	public String[] getUploadFileName() {
		return uploadFileName;
	}
	public void setUploadFileName(String[] uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	
	public String execute(){
		BufferedInputStream bis = null ; 
		BufferedOutputStream bos = null ; 
		String savepath = ServletActionContext.getServletContext().getRealPath("/") ;
		filepaths = new ArrayList<String>();
		try{
		for (int i = 0; i < files.length; i++) {
			savepath = savepath+"/" +UUID.randomUUID().toString() +filesFileName[i] ; 
			bis = new BufferedInputStream(new FileInputStream(files[i])) ;
			bos = new BufferedOutputStream(new FileOutputStream(savepath));
			int temp = 0 ; 
			byte[] buffer = new byte[1024] ;
			while((temp = bis.read(buffer))>0){
				bos.write(buffer,0,temp);
			}
			bos.flush();
			filepaths.add(savepath);					//把服务器上的文件的地址放到数组里面
		}	
		bis.close();
		bos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		return SUCCESS;
	} 
	
	/*
	 * 多文件上传
	 * wuqihan
	 */
	public String multiFileUpload(){
		
				String path = ServletActionContext.getServletContext().getRealPath("/upload/");
				FileService.getFilelist().clear();
				if(upload!=null){
					File saveDir = new File(path);
					if(!saveDir.exists()){
						saveDir.mkdirs();
					}
					/*al.add("123");*/
					
					
				
					
					for(int i=0;i<uploadFileName.length;i++){
						File saveFile = new File(saveDir,uploadFileName[i]);
						try {
							FileUtils.copyFile(upload[i], saveFile);
							
					
							FileService.getFilelist().add(path+"/"+uploadFileName[i]);
							
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}	
			

		//此处会根据上传的类型判断时候需要跳转 uptype == onlyup ->不需要   uptype == need ->需要跳转
			if(uptype.equals("need")){
				return "multiFileUpload";
			}else{				
				
				return "onlyup" ;
			}
		}
	
}
